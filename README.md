# GFS PorsonMod

A modified version of the GFS Porson typeface, designed by George Matthiopoulos for the [Greek Font Society](http://greekfontsociety-gfs.gr), which contains the necessary glyphs for ancient greek epigraphy and critical editions, and also adds certain Open Type features, such as the `mark` tag, to control the correct positioning of the Unicode character `U+0323`: Combining Dot Below (see file test.tex, which can be compiled with XeLaTeX or LuaLaTeX).

The file GFS PorsonMod-Regular.sfd can be edited with [Fontforge](https://fontforge.github.io/en-US/).
